# Flathub Docker config

## Pre-requirements:
* X11 forwarding enabled on Client and Server.

## Usage:
* Build docker container ```docker build -t flathub .```
* Run with parent script ```docker-run flathub```

## Enable X11 forwarding
##### Server
* Install pre-reqs```yum install  xorg-x11-server-Xorg xorg-x11-xauth xorg-x11-apps -y```
* In `/etc/ssh/sshd_config` Set ```X11Forwarding yes```
* Restart the ssh daemon `systemctl restart sshd`

##### Client
* In `/etc/ssh/sshd_config` uncomment ```ForwardX11 yes```
